using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
   public  int score;
    public static GameManager inst;
    public Text ScoreText;
    public void IncrementScore()
    {
        score++;
        ScoreText.text = "SCORE: " + score;
        // Increase the player's speed
        //playerMovement.speed += playerMovement.speedIncreasePerPoint;
    }
    private void Awake()
    {
        inst = this;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
