using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    playerMovement playerMovement;
    void Start()
    {
        playerMovement = GameObject.FindObjectOfType<playerMovement>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "player")
        {
            // Kill the player
            playerMovement.Die();
        }
    }
    void Update()
    {
        
    }
}
